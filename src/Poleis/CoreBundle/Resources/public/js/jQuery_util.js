(function($){
	$.isBlank = function(obj) {
		return(!obj || $.trim(obj) === "");
	};
	$.altValue = function(val, defaultVal) {
		if(val && !$.isBlank(val)) {
			return val;
		}
		return defaultVal;
	};
	$.idify = function(id) {
		return id.replace(/\W+/g, '');
	};
})(jQuery);