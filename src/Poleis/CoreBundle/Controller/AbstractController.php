<?php
namespace Poleis\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Poleis\CoreBundle\Entity\AbstractEntity\BasicDBEntity;
use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;
use Doctrine\ORM\ORMException;
use Poleis\CoreBundle\Entity\Organization;
use Poleis\CoreBundle\Entity\School;
use Poleis\CoreBundle\Entity\Chapter;
use Poleis\CoreBundle\Entity\Member;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;


/**
 *  Methods in this class should abstract nearly all
 *  ORM interaction.
 *  
 * @author Timothy Stephens <timothy.f.stephens@gmail.com>
 * @version 02.18.2014
 */

abstract class AbstractController extends Controller {

	private
	$entityManager,
	$aclManager,
	$securityContext;
	
	//
	//
	// Entity Management
	//
	//
	
	protected function persist(BasicDBEntity $e, $autoFlush = false) {
		$repo = $this->getRepository($e);
		
		if($repo instanceof AbstractRepository) {
			return $repo->persist($e, $autoFlush);
		} else { // less efficient
			$this->getEntityManager()->persist($e);
			if($autoFlush) {
				$this->getEntityManager()->flush();
			}
			return $e;
		}
	}
	
	protected function saveEntity(BasicDBEntity $e) {
		return $this->persist($e, true);
	}

	/**
	 * Can be used in multiple ways.  $entity can be a string or an Entity.
	 * If $id, it looks for an entity with that id.  If $entity is an entity
	 * with an $id, then $id does not need to be set.
	 * 
	 * @param Request $request
	 * @param mixed $type
	 * @param mixed $entity
	 * @param int $id
	 * @return multitype:NULL BasicDBEntity \Symfony\Component\Form\Form
	 */
	protected function resolveForm(Request $request, $type, $entity, $id = -1) {
		//TODO should be updated to handle both creation and updates
		// (getting lifecycle callbacks and auditing to work may solve this)
		
		if($id > 0) {
			if($entity instanceof BasicDBEntity && $entity->getId() > 0 ) {
				// do nothing, we already have an entity
			} else {
				$entity = $this->getRepository($entity)->find($id);
			}
		} else if (is_string($entity)) {
			$entity = new $entity();
		}
		
		$form = $this->createForm($type, $entity);
		
		$form->handleRequest($request);
		
  		return array("success" => $form->isValid(), "entity" => $this->persist($entity), "form" => $form);
	}

	/**
	 * Looks for an object of the given type (path or object) with the given criteria (id or array with parameters).
	 * Returns that object or a new one if not found.
	 * @param BasicDBEntity or String $obj
	 * @param int $id
	 */
	protected function loadOrCreate($obj, $criteria) {
		if(is_array($criteria)) {
			$newObj = $this->getRepository($obj)->findOneBy($criteria);
		} else if(is_int($criteria)) {
			$newObj = $this->getRepository($obj)->find($criteria);
		} else {
			throw new \InvalidArgumentException("Criteria was not an ID or array of parameters");
		}
		
		if(empty($newObj)) {
			if(is_string($obj)) {
				return $this->persist(new $obj());
			}
			return $this->persist($obj);
		}
		return $newObj;
	}
	
	public function getEntityManager() {
		return $this->entityManager;
	}
	public function setEntityManager($entityManager) {
		$this->entityManager = $entityManager;
	}
	public function getAclManager() {
		return $this->aclManager;
	}
	public function setAclManager($aclManager) {
		$this->aclManager = $aclManager;
	}
	public function getSecurityContext() {
		return $this->securityContext;
	}
	public function setSecurityContext($securityContext) {
		$this->securityContext = $securityContext;
	}
 
 
}