<?php
//src/Poleis/CoreBundle/Controller/SchoolController

namespace Poleis\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security; 
use Symfony\Component\HttpFoundation\Request;
use Poleis\CoreBundle\Entity\School;
use Poleis\CoreBundle\Form\Type\SchoolType;
use Doctrine\Common\Collections\ArrayCollection;
use Poleis\CoreBundle\Repository\SchoolRepository;

/**
 * @Route("/school", service="poleis.controller.School")
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 03.14.2014
 */
class SchoolController extends AbstractController {
	private $schoolRepository;

	/**
	 * View the a list of all schools on the site, with links to edit or view the schools, and the school's homepage
	 *
	 * @Route("/", name="school")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction() {
		
		// can use rolesecurityidentity instead of usersecurityidentity to set
		// acl for roles.  roles should be community specific
		// that way you only have access to stuff in your community
		
		$this->getSecurityContext()->isGranted("VIEW", new School());
		return array("schools" => $this->getSchoolRepository()->findAll());
	}
	
	/**
	 * @Route("/", name="school_create")
	 * @Method("POST")
	 * @Template("PoleisCoreBundle:School:new.html.twig")
	 */
	public function createAction(Request $req) {
		$result = $this->resolveForm($req, 'School', new School());
	
		if($result['success']) {
			$this->getEntityManager()->flush();
			return $this->redirect($this->generateUrl('school_show', array('id' => $result['entity']->getId())));
		}
		return array(
			'entity' => $result['entity'],
			'schoolForm' => $result['form']->createView(),
			'errors' => $result['form']->getErrors()
		);
	}
	
	/**
	 * Create a new school
	 * 
	 * @Route("/new", name="school_new")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction() {
		return array('schoolForm' => $this->createForm(
			'School',
			new School(),
			array('action' => $this->generateUrl('school_create'), 'method' => 'POST')
		)->createView());
	}
	
	/**
	 * View the school's description on the site, with links to the school's homepage
	 *
	 * Most other routes should probably be secured to superadmin
	 *
	 * @Route("/{id}", name="school_show")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction($id) {
		if (is_numeric($id)) {
			try {
				return array("school" => $this->getSchoolRepository()->find($id));
			} catch (\Exception $e) {
				// do something
				throw $e;
			}
		} else {
			throw $this->createNotFoundException("Could not find school with id: {$id}");
		}
	}
		
	/**
	 * @Route("/{id}/edit", name="school_edit")
	 * @Method("GET")
	 * @Template("PoleisCoreBundle:School:edit.html.twig")
	 */
	public function editAction($id) {
		return array('schoolForm' => $this->createForm(
			'School',
			$this->getSchoolRepository()->find($id),
			array(
					'action' => $this->generateUrl('school_update', array('id' => $id)),
					'method' => 'POST'
				)
			)->createView()
		);		
	}

	/**
	 * @Route("/{id}", name="school_update")
	 * @Method({"PUT","POST"})
	 * @Template("PoleisCoreBundle:School:edit.html.twig")
	 */
	public function updateAction(Request $req, $id) {
		$school = $this->getSchoolRepository()->find($id);
	
		$nicknames = new ArrayCollection();
		foreach($school->getNicknames() as $nn) {
			$nicknames->add($nn);
		}
		$emailSuffixes = new ArrayCollection();
		foreach($school->getEmailSuffixes() as $es) {
			$emailSuffixes->add($es);
		}
		
		$result = $this->resolveForm($req, 'School', $school);
		if($result['success']) {
			foreach($nicknames as $nickname) {
				if(false === $school->getNicknames()->contains($nickname)) {
					$this->getEntityManager()->remove($nickname);
				}
			}
			foreach($emailSuffixes as $emailSuffix) {
				if(false === $school->getEmailSuffixes()->contains($emailSuffix)) {
					$this->getEntityManager()->remove($emailSuffix);
				}
			}
			
			$this->getEntityManager()->flush();
			return $this->redirect($this->generateUrl('school'));
		}
		return array(
				'entity' => $result['entity'],
				'schoolForm' => $result['form']->createView(),
				'errors' => $result['form']->getErrors()
		);
	}
		
	/**
	 * Set the user's school
	 *
	 * @Route("/{id}/set", name="school_set")
	 */
	public function setAction($id) {
		$currSchool = $this->getUser()->getCommunity();
		$school = $this->getSchoolRepository()->find($id);
		if(empty($currSchool)) {
			if(!empty($school)) {
				$this->getUser()->setCommunity($school);
			} else {
				throw $this->createNotFoundException("Could not find School with id: $id");
			}
		} else {
			// throw exception "User already has a community set."
			// this may ultimately need to change as the application grows and changes
		}
	}
	
	/**
	 * Load all schools.  If 'prefix' is set, load just the schools that have a name or nickname
	 * that start with that prefix (AJAX)
	 * 
	 * @Route("/get/{prefix}", name="school_get")
	 */
	public function getAllAction() {

	}
	
	public function getSchoolRepository() {
		return $this->schoolRepository;
	}
	public function setSchoolRepository(SchoolRepository $schoolRepository) {
		$this->schoolRepository = $schoolRepository;
	}
}