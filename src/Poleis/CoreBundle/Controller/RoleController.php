<?php

namespace Poleis\CoreBundle\Controller;

use Poleis\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Deal with all things role related
 * 
 * @Route("/role")
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version Apr 25, 2014
 *
 */
class RoleController extends AbstractController {
	
	private $roleRepository;
	
	/**
	 * List roles
	 * 
	 * @Route("/", name="role")
	 * @Method("GET")
	 * @Template()
	 */
	public function indexAction() {
		
	}

}