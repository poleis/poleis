<?php
//src/Poleis/CoreBundle/Controller/AdminController

namespace Poleis\CoreBundle\Controller;

use Poleis\CoreBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Poleis\CoreBundle\Entity\EntityInterface\Permissible;

/**
 * @Route("/admin")
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version Apr 17, 2014
 */
class AdminController extends AbstractController {
	
	private $memberRepository;
	
	/**
	 * Edit user permissions
	 * @Route("/permissions", name="admin_permissions")
	 * @Method("GET")
	 * @Template()
	 */
	public function permissionsAction() {
		return array(
		 	'members' => $this->memberRepository->findAll(),
			'entities' => array_filter(get_declared_classes(), // filter to only classes that implement Poleis\CoreBundle\Entity\EntityInterface\Permissible
					function($className) {
						return in_array('Permissible', class_implements($className));
					})
		);
	}
	

}