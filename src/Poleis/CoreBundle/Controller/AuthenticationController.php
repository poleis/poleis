<?php

namespace Poleis\CoreBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Poleis\CoreBundle\Form\Type\MemberRegistrationType;
use Poleis\CoreBundle\Entity\Member;

/**
 * @Route("/auth")
 * 
 * TODO update with better CRUD compliance
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version Jan 8, 2014
 */
class AuthenticationController extends AbstractController
{
    /**
     * This is where the form is displayed
     * 
     * @Route("/login", name="login")
     * @Template("PoleisCoreBundle:Authentication:login.html.twig")
     */
    public function loginAction(Request $request)
    {
    	$session = $request->getSession();
    	
    	// get the login error if there is one
    	if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
    		$error = $request->attributes->get(
    				SecurityContext::AUTHENTICATION_ERROR
    		);
    	} else {
    		$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
    		$session->remove(SecurityContext::AUTHENTICATION_ERROR);
    	}
    	
    	return array(
    			// last email entered by the user
    			'lastEmail' => $session->get(SecurityContext::LAST_USERNAME),
    			'error'     => $error,
		    	);
    }

    /**
     * This is where the actual authentication is verified
     * @Route("/login_check", name="auth_verify")
     */
    public function loginCheckAction() {
    }
    
    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction() {
    	
    }
    
    /**
     * @Route("/register", name="register")
     * @Template()
     */
    public function registerAction($error = "") {
    	return array(
    			'registerForm' => $this->createForm(
		    		'MemberRegistration',
		    		new Member(),
		    		array('action' => $this->generateUrl('member_register'))
	    		)->createView()
	    	);
    }
    
    /**
     * @Route("/create", name="member_register")
     * @Template("PoleisCoreBundle:Authentication:register.html.twig")
     */
    public function createAction(Request $req) {
		$result = $this->resolveForm($req, 'MemberRegistration', new Member());

		if($result['success']) {
    		$member = $result['entity'];
    		$member->setPassword(
    			$this->get('security.encoder_factory')
    				->getEncoder($member)
    				->encodePassword($member->getPassword())
    		);
    		
    		// should figure out created_by here.  if done by referral, created by is the referring member
    		// otherwise its the default?
    		
    		$this->saveEntity($member);
    		
    		return $this->redirect($this->generateUrl("recruitment"));
    	}
		return array(
			'entity' => $result['entity'],
			'registerForm' => $result['form']->createView(),
			'errors' => $result['form']->getErrors()
		);
    }

}
