<?php
//src/Poleis/CoreBundle/AbstractRepository.php
namespace Poleis\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Poleis\CoreBundle\Entity\AbstractEntity\BasicDBEntity;
use Poleis\CoreBundle\Entity\Member;
use Poleis\CoreBundle\Entity\Organization;

use Doctrine\ORM\ORMException;

/**
 * Should probably have a few other repository to extend
 * IE, Contactable to load a ContactableEntity by its facebook or twitter id
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 04.08.2014
 */
abstract class AbstractRepository extends EntityRepository {
	const UOW_SIZE = 10; // TODO should be a parameter in YAML

	/**
	 * Can do some more magic here later
	 * 
	 * @param string|BasicDBEntity $e
	 * @return BasicDBEntity
	 */
	public function persist($e, $autoFlush = false) {
		if(is_array($e)) {
			foreach($e as $entity) {
				$this->persist($entity);
			}
		} else if ($e instanceof BasicDBEntity && !$this->_em->contains($e)) {
			$this->_em->persist($e);
		}
		
		$this->flush($autoFlush);
		
		return $e;
	}
	
	protected function flush($force) {
		if($force){// || true || $this->_em->getUnitOfWork()->getSize() > UOW_SIZE) {
			$this->_em->flush();
		}
	}
}