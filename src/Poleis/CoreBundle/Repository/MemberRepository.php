<?php
//src/Poleis/CoreBundle/Repository/MemberRepository.php
namespace Poleis\CoreBundle\Repository;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\ORMException;
use Poleis\CoreBundle\Entity\Member;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * 
 * @author timothy
 */

class MemberRepository extends AbstractRepository implements UserProviderInterface {
	
	/**
	 * @see \Symfony\Component\Security\Core\User\UserProviderInterface::loadUserByUsername()
	 */
	public function loadUserByUsername($email) {
		try {
			return $this->createQueryBuilder('m')->where('m.email = :email')
					->setParameter('email', $email)
					->getQuery()
					->getSingleResult();
		} catch (NoResultException $e) {
			throw new UsernameNotFoundException(
				sprintf('Unable to find a member with email: %s', $email),
				0, $e);
		}
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\UserProviderInterface::refreshUser()
	 */
	public function refreshUser(UserInterface $user) {
		if(!$this->supportsClass(get_class($user))) {
			throw new UnsupportedUserException(
				sprintf('Instance of "Member" expected, instance of "%s" found', get_class($user))
			);
		}
		return $this->find($user->getId());		
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\UserProviderInterface::supportsClass()
	 */
	public function supportsClass($class) {
		return $this->getEntityName() === $class ||
			is_subclass_of($class, $this->getEntityName());
	}
}


































