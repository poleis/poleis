<?php

namespace Poleis\CoreBundle\Security;

use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class PoleisRoleHierarchy extends RoleHierarchy {
	private $entityManager,
	/**
	 * @var NestedTreeRepository
	 */
	$roleRepository;
	
	public function __construct() {
		parent::__construct($this->buildRoleMap());
	}
	
	/*
	 * Build map
	 */
	private function buildRoleMap() {
		$roles = $this->roleRepository->findAll();
	}

	public function getEntityManager() {
		return $this->entityManager;
	}
	public function setEntityManager($entityManager) {
		$this->entityManager = $entityManager;
	}
	public function getRoleRepository() {
		return $this->roleRepository;
	}
	public function setRoleRepository($roleRepository) {
		$this->roleRepository = $roleRepository;
	}
}