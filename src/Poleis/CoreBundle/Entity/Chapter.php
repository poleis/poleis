<?php
//src/Poleis/CoreBundle/Entity/Chapter

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Poleis\CoreBundle\Entity\AbstractEntity\Community;
use Poleis\CoreBundle\Entity\EntityInterface\Permissible;

/**
 * @Entity
 * @Table(uniqueConstraints={
 * 	@UniqueConstraint(name="uq_chapter", columns={
 * 		"org_id", "school_id"
 * 	})
 * })
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

class Chapter extends Community implements Permissible {
	protected
	/**
	 * @ManyToOne(targetEntity="Organization", fetch="LAZY")
	 * @JoinColumn(name="org_id", referencedColumnName="id", nullable=false)
	 */
	$organization,
	/**
	 * @ManyToOne(targetEntity="School", fetch="LAZY")
	 * @JoinColumn(name="school_id", referencedColumnName="id", nullable=false)
	 */
	$school;
	
	public function isSubCommunityOf(Community $community) {
		return $community->equals($this->organization) || $community->equals($this->school);
	}
	
	public function addMember(Member $member) {
		if(!$this->hasMember($member)) {
			$organization->addMember($member);
			$school->addMember($member);
			$this->addMemberHelper($member);
		}
	}
	
	public function __construct() {
		parent::__construct();
	}
	public function getOrganization() {
		return $this->organization;
	}
	public function setOrganization($organization) {
		$this->organization = $organization;
	}
	public function getSchool() {
		return $this->school;
	}
	public function setSchool($school) {
		$this->school = $school;
	}
}