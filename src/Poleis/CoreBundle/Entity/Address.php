<?php
//src/Poleis/CoreBundle/Entity/Address
namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;

/**
 * @Entity
 * @Table()
 * 
 * @author Timothy Stephens
 * @version 01.06.14
 */

class Address extends AuditableEntity {
	protected
	/**
	 * @Column(type="string", name="addr_1", nullable=false)
	 * @var String
	 */
	$address1,
	/**
	 * @Column(type="string", name="addr_2")
	 * @var String
	 */
	$address2,
	/**
	 * @Column(type="string", nullable=false)
	 * @var String
	 */
	$city,
	/**
	 * @Column(type="string", length=2, nullable=false)
	 * @var String
	 */
	$state,
	/**
	 * @Column(type="integer", nullable=false, name="zip_code")
	 * @var int
	 */
	$zip;
}