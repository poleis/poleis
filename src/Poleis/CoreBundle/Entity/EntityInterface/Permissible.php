<?php
//src/Poleis/CoreBundle/Entity/EntityInterface/Permissible

namespace Poleis\CoreBundle\Entity\EntityInterface;

/**
 * Marker Interface for Classes that can have ACLs defined on them
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version Apr 20, 2014
 *
 */
interface Permissible {
	// should provide information about which roles have what permissions on the entity?
	// also could just be a tagger interface
}