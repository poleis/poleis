<?php
namespace Poleis\CoreBundle\Entity\EntityInterface;

use Poleis\CoreBundle\Entity\AbstractEntity\BasicDBEntity;
/**
 * Interface for comparison and equality
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 04.10.2014
 */
interface Comparable {
	public function compareTo(BasicDBEntity $entity);
	public function equals(BasicDBEntity $entity);
}