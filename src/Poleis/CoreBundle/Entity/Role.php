<?php
//src/Poleis/CoreBundle/Entity/Role

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Gedmo\Mapping\Annotation\Tree;
use Gedmo\Mapping\Annotation\TreeLeft;
use Gedmo\Mapping\Annotation\TreeRight;
use Gedmo\Mapping\Annotation\TreeRoot;
use Gedmo\Mapping\Annotation\TreeLevel;
use Gedmo\Mapping\Annotation\TreeParent;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OrderBy;
use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;
use Doctrine\ORM\Mapping\ManyToMany;

/**
 * @Entity(repositoryClass="Gedmo\Tree\Entity\Repository\NestedTreeRepository")
 * @Table()
 * @Tree(type="nested")
 * @author Timothy Stephens
 * @version 01.28.14
 */

final class Role extends AuditableEntity implements RoleInterface {
	private
	/**
	 * @Column(type="string", length=30)
	 */
	$name,
	/**
	 * @Column(type="string", length=20, unique=true)
	 */
	$role,
	/**
	 * @TreeRoot
	 * @Column(type="integer", nullable=true)
	 */
	$root,
	/**
	 * @TreeLevel
	 * @Column(type="integer")
	 */
	$level,
	/**
	 * @TreeParent
	 * @ManyToOne(targetEntity="Role", inversedBy="children")
	 * @JoinColumn(referencedColumnName="id", onDelete="CASCADE")
	 * @var Role
	 */
	$parent,
	/**
	 * @OneToMany(targetEntity="Role", mappedBy="parent")
	 * @OrderBy({"left"="ASC"})
	 */
	$children,
	/**
	 * @TreeLeft
	 * @Column(name="left_pos", type="integer")
	 * @var integer
	 */
	$left,
	/**
	 * @TreeRight
	 * @Column(name="right_pos", type="integer")
	 */
	$right,
	/**
	 * @ManyToOne(targetEntity="Poleis\CoreBundle\Entity\AbstractEntity\Community", inversedBy="roles")
	 * @JoinColumn(referencedColumnName="community_id", onDelete="CASCADE")
	 */
	$community,
	/**
	 * @ManyToMany(targetEntity="Member", mappedBy="roles". fetch="EXTRA_LAZY")
	 */
	$members;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getRole() {
		return $this->role;
	}
	public function setParent(Role $parent = null) {
		if($parent->community != null) {
			$this->community = $parent->community;
		}
		$this->parent = $parent;
	}
	public function getParent() {
		return $this->parent;
	}
}