<?php
//src/Poleis/CoreBundle/Entity/Nickname

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;

/**
 * @Entity
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 03.16.14
 */

class Nickname extends AuditableEntity {
	private
	/** 
	 * @ManyToOne(targetEntity="Poleis\CoreBundle\Entity\AbstractEntity\Community", fetch="EAGER", cascade={"persist"}, inversedBy="nicknames")
	 */
	$community,
	/** @Column(type="string") @var string */
	$nickname;
	
	public function __construct() {
		parent::__construct();
	}
		
	public function getNickname() {
		return $this->nickname;
	}
	public function setNickname($nickname) {
		$this->nickname = $nickname;
	}
}
