<?php
//src/Poleis/CoreBundle/Entity/Abstract/AddressableEntity
namespace Poleis\CoreBundle\Entity\AbstractEntity;

use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @MappedSuperclass
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

abstract class AddressableEntity extends AuditableEntity {
	/**
	 * @OneToOne(targetEntity="Poleis\CoreBundle\Entity\Address", fetch="LAZY", orphanRemoval=true)
	 * @JoinColumn(referencedColumnName="id")
	 */
	private $address;
	
	protected function __construct() {
		parent::__construct();
	}
	public function getAddress() {
		return $this->address;
	}
	public function setAddress($address) {
		$this->address = $address;
	}
}