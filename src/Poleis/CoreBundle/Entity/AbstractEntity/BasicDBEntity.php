<?php
//src/Poleis/CoreBundle/Entity/AbstractEntity/BasicDBEntity

namespace Poleis\CoreBundle\Entity\AbstractEntity;

use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @MappedSuperclass
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

abstract class BasicDBEntity {
	protected 
	/**
	 * @Column(type="integer", nullable=false)
	 * @Id
	 * @GeneratedValue(strategy="SEQUENCE")
	 * @var int
	 */
	$id,
	/**
	 * @Column(type="boolean", name="active", nullable=false)
	 * @var boolean
	 */
	$isActive;
	
	protected function __construct() {
		// all new entities start as active unless otherwise specified
		$this->isActive = true;
	}
	
	public function getId() {
		return $this->id;
	}
	public function setId($id) {
		$this->id = $id;
	}
	
	public function isActive() {
		return $this->isActive;
	}
	public function getIsActive() {
		return $this->isActive();
	}
	public function setActive($isActive) {
		$this->isActive = $isActive;
	}
}