<?php
//src/Poleis/CoreBundle/Entity/AbstractEntity/SocialMediaEntity
namespace Poleis\CoreBundle\Entity\AbstractEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Column;

/**
 * @MappedSuperclass
 * @DoctrineAssert\UniqueEntity(fields="facebook", message="This facebook user is already registered.")
 * @DoctrineAssert\UniqueEntity(fields="twitter", message="This twitter user is already registered.")
 *
 * @author Timothy Stephens
 * @version 01.05.14
 */

abstract class ContactableEntity extends AddressableEntity {
	private
	/** @Column(name="fb_id", type="integer", unique=true, nullable=true) @var int */
	$facebook,
	/** @Column(name="twitter_id", type="string", unique=true, nullable=true) @var string */
	$twitter,
	/**
	 * @Column(type="string", unique=true, nullable=true)
	 * @Assert\Email()
	 * @var string
	 */
	$email; 

	protected function __construct() {
		parent::__construct();
	}
	public function getFacebook() {
		return $this->facebook;
	}
	public function setFacebook($facebook) {
		$this->facebook = $facebook;
		return $this;
	}
	public function getTwitter() {
		return $this->twitter;
	}
	public function setTwitter($twitter) {
		$this->twitter = $twitter;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
 }