<?php
//src/Poleis/CoreBundle/Entity/AbstractEntity/AuditableEntity
namespace Poleis\CoreBundle\Entity\AbstractEntity;

use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\PreUpdate;
use Doctrine\ORM\Mapping\PrePersist;
use Gedmo\Mapping\Annotation\Timestampable;
use Gedmo\Mapping\Annotation\Blameable;
use Gedmo\Mapping\Annotation\SoftDeleteable;
use Poleis\CoreBundle\Entity\Member;

/**
 * @MappedSuperclass
 * @SoftDeleteable(fieldName="deletedDate", timeAware=true)
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

abstract class AuditableEntity extends BasicDBEntity {
	private 
	/** @ManyToOne(targetEntity="Poleis\CoreBundle\Entity\Member", cascade={"persist"}, fetch="LAZY")
	 * @JoinColumn(name="created_by", referencedColumnName="id")
	 * , nullable=false)
	 * @Blameable(on="create")
	 * @var Member
	 */
	$createdBy,
	/** 
	 * @Column(type="datetimetz", name="created_date", nullable=false)
	 * @Timestampable(on="create")
	 * 
	 * @var \DateTime $createdDate
	 */
	$createdDate,
	/** @ManyToOne(targetEntity="Poleis\CoreBundle\Entity\Member", cascade={"persist"}, fetch="LAZY")
	 * @JoinColumn(name="updated_by", referencedColumnName="id")
	 * , nullable=false)
	 * @Blameable(on="update")
	 * @var Member
	 */
	$updatedBy,
	/** 
	 * @Column(type="datetimetz", name="updated_date", nullable=false)
	 * @Timestampable(on="update")
	 * @var \DateTime $updatedDate
	 */
	$updatedDate,
	/**
	 * @Column(name="deleted_date", type="datetimetz", nullable=true)
	 * @var \DateTime $deletedDate
	 */
	$deletedDate;

	protected function __construct() {
		parent::__construct();
	}
	
	public function getCreatedBy() {
		return $this->createdBy;
	}
	public function setCreatedBy(Member $createdBy) {
		$this->createdBy = $createdBy;
	}
	public function getUpdatedBy() {
		return $this->updatedBy;
	}
	public function setUpdatedBy(Member $updatedBy) {
		$this->updatedBy = $updatedBy;
	}
	public function getCreatedDate() {
		return $this->createdDate;
	}
	public function setCreatedDate($createdDate) {
		$this->createdDate = $createdDate;
	}
	public function getUpdatedDate() {
		return $this->updatedDate;
	}
	public function setUpdatedDate($updatedDate) {
		$this->updatedDate = $updatedDate;
	}
	public function getDeletedDate() {
		return $this->deletedDate;
	}
	public function setDeletedDate($deletedDate) {
		$this->deletedDate = $deletedDate;
	}
	public function undelete() {
		$this->setDeletedDate(null);
	}

}
