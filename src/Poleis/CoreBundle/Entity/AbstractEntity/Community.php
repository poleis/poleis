<?php
//src/Poleis/CoreBundle/Entity/AbstractEntity/Community

namespace Poleis\CoreBundle\Entity\AbstractEntity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;
use Poleis\CoreBundle\Entity\EntityInterface\Comparable;
use Poleis\CoreBundle\Entity\Nickname;
use Poleis\CoreBundle\Entity\Chapter;
use Poleis\CoreBundle\Entity\School;
use Poleis\CoreBundle\Entity\Member;
use Doctrine\ORM\Mapping\ManyToMany;
use Poleis\CoreBundle\Entity\Organization;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="community_type")
 * @DiscriminatorMap({
 * "organization" = "Poleis\CoreBundle\Entity\Organization",
 * "chapter"      = "Poleis\CoreBundle\Entity\Chapter",
 * "school"       = "Poleis\CoreBundle\Entity\School"
 * })
 * Eventually committee and council will be added.
 * Committee is a child of any community.
 * Council is an inter-community group, with restricted access to those communities
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 01.05.14
 */

abstract class Community extends ContactableEntity implements Comparable {
	private
	/** @Column(nullable=false, type="string") @var string */
	$name,
	/** @Column(nullable=true, type="string") @var string */
	$webpage,
	/**
	 * @OneToMany(targetEntity="Poleis\CoreBundle\Entity\Nickname",
	 * 			  fetch="EAGER", mappedBy="community",
	 * 			  cascade={"all"})
	 * 
	 * @var ArrayCollection
	 */
	$nicknames,
	/**
	 * @ManyToMany(targetEntity="Poleis\CoreBundle\Entity\Member", mappedBy="communities", fetch="EXTRA_LAZY")
	 */
	$members;
	
	protected function __construct() {
		parent::__construct();
		
		$this->nicknames = new ArrayCollection();
		$this->members = new ArrayCollection();
	}

	abstract public function isSubCommunityOf(Community $community);
	abstract public function addMember(Member $member);
	 
	public function isAncestorCommunity(Community $community) {
		return $community->isSubCommunity($this);
	}
	
	protected final function addMemberHelper(Member $member) {
		if(!$this->hasMember($member)) {
			$this->members->add($member);
		}
	}
	public function hasMember(Member $member) {
		return $this->members->contains($member);
	}
	
	public function compareTo(BasicDBEntity $community) {
		if($community instanceof Community) {
			if($this->equals($community)) {
				return 0;
			} else if ($this->isSubCommunity($community)) {
				return -1;
			} else if ($this->isAncestorCommunity($community)) {
				return 1;
			}
		}
		return false;
	}
	
	public function equals(BasicDBEntity $community) {
		return (($community instanceof Community) && (get_class($this) == get_class($community))) && ($this->getId() == $community->getId());
	}
	
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function getWebpage() {
		return $this->webpage;
	}
	public function setWebpage($webpage) {
		$this->webpage = $webpage;
	}
	public function getNicknames() {
		return $this->nicknames;
	}
	public function setNicknames(ArrayCollection $nicknames) {
		$this->nicknames = $nicknames;
	}
	public function addNickname(Nickname $n) {
		$n->setCommunity($this);
		$this->nicknames->add($n);
	}
	public function removeNickname(Nickname $n) {
		$this->nicknames->removeElement($n);
	}
	public function isSchool() {
		return $this instanceof School;
	}
	public function isOrganization() {
		return $this instanceof Organization;
	}
	public function isChapter() {
		return $this instanceof Chapter;
	}

}
