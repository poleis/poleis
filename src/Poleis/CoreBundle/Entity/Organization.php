<?php
//src/Poleis/CoreBundle/Entity/Organization

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Poleis\CoreBundle\Entity\AbstractEntity\Community;
use Poleis\CoreBundle\Entity\EntityInterface\Permissible;

/**
 * @Entity
 * @Table()
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

class Organization extends Community implements Permissible {
	
	public function __construct() {
		parent::__construct();

	}

	public function isSubCommunityOf(Community $community) {
		return false; // organization is always a parent community
	}
	
	/**
	 * @see \Poleis\CoreBundle\Entity\AbstractEntity\Community::addMember()
	 */
	public function addMember(Member $member) {
		$this->addMemberHelper($member);
	}

}