<?php
//src/Poleis/CoreBundle/Entity/Member

namespace Poleis\CoreBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\NamedNativeQueries;
use Doctrine\ORM\Mapping\NamedNativeQuery;
use Doctrine\ORM\Mapping\PrePersist;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Poleis\CoreBundle\Entity\AbstractEntity\ContactableEntity;
use Poleis\CoreBundle\Entity\AbstractEntity\Community;

/**
 * @Entity(repositoryClass="Poleis\CoreBundle\Repository\MemberRepository")
 * @Table()
 * @DoctrineAssert\UniqueEntity(fields="email", message="Email already exists in Member.")
 * @HasLifecycleCallbacks
 * 
 * The default user
 *
 * @author Timothy Stephens
 * @version 01.05.14
 */

class Member extends ContactableEntity implements AdvancedUserInterface, \Serializable, EquatableInterface {
	protected
	/**
	 * @Column(name="first_name", nullable=false, type="string")
	 * @Assert\NotBlank()
	 * @var string
	 */
	$firstName,
	/**
	 * @Column(name="last_name", nullable=false, type="string")
	 * @Assert\NotBlank()
	 * @var string
	 */
	$lastName,
	/**
	 * @Column(name="password", nullable=false, type="string")
	 * @Assert\NotBlank()
	 * @Assert\Length(max=1024)
	 * @var string
	 */
	$password,
	/** 
	 * @ManyToMany(targetEntity="Poleis\CoreBundle\Entity\AbstractEntity\Community", inversedBy="members", fetch="EAGER")
	 * @JoinTable(name="community_membership",
	 * 	joinColumns={@JoinColumn(name="member_id", referencedColumnName="id")},
	 *  inverseJoinColumns={@JoinColumn(name="community_id", referencedColumnName="id")}
	 * )
	 * 
	 * Would prefer to have this as a composite key/index IE unique(member_id, community_id)
	 * 
	 * @var ArrayCollection $communities
	 */
	$communities,
	/** @ManyToMany(targetEntity="Role", inversedBy="members", fetch="EAGER")
	 *  @JoinTable(name="member_roles",
	 *  	joinColumns={
	 *  		@JoinColumn(name="member_id", referencedColumnName="id")},
	 *  	inverseJoinColumns={
	 *  		@JoinColumn(name="role_id", referencedColumnName="id")}
	 *  )
	 *  
	 *  @var ArrayCollection $roles
	 */
	$roles;

	public function __construct() {
		parent::__construct();
		
		$roles = new ArrayCollection();
		$communities = new ArrayCollection();
	}

	/**
	 * @inheritDoc
	 */
	public function getUsername() {
		return $this->getEmail();
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isAccountNonExpired()
	 */
	public function isAccountNonExpired() {
		return $this->isActive;
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isAccountNonLocked()
	 */
	public function isAccountNonLocked() {
		return $this->isActive;
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isCredentialsNonExpired()
	 */
	public function isCredentialsNonExpired() {
		return $this->isActive;
	}

	/**
	 * @see \Symfony\Component\Security\Core\User\AdvancedUserInterface::isEnabled()
	 */
	public function isEnabled() {
		return $this->isActive;
	}
	public function getFirstName() {
		return $this->firstName;
	}
	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}
	public function getLastName() {
		return $this->lastName;
	}
	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}
	public function setPassword($password) {
		$this->password = $password;
	}
	public function getCommunities() {
		return $this->communities;
	}
	public function setCommunities($communities) {
		$this->communities = $communities;
	}
	public function setRoles($roles) {
		$this->roles = $roles;
	}
 	public function addRole(Role $role) {
 		$this->roles->add($role);
 	}
	public function setName($last, $first=NULL) {
		if(empty($first) && strstr($last, " ")) {
			// TODO split stuff here
			// should be able to figure out if its last, first
			// or first last
			// prototype should probably take last, first
		}
		$this->setFirstName($first);
		$this->setLastName($last);
	}
	
	/**
	 * @inheritDoc
	 */
	public function getSalt() {
		return null;
	}

	/**
	 * @inheritDoc
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @inheritDoc
	 */
	public function getRoles() {
		$roles = $this->roles->toArray();
		$roles[] = 'ROLE_MEMBER';
		return $roles;
	}

	/**
	 * @inheritDoc
	 */
	public function eraseCredentials() {

	}

	/**
	 * @param Community $community
	 * @return boolean
	 */
	public function isMemberOf(Community $community) {
		return $this->communities->contains($community);
	}
	
	/**
	 * @see \Serializable::serialize()
	 */
	public function serialize() {
		return serialize(array(
				$this->id,
				$this->getEmail(),
				$this->password,
				$this->firstName,
				$this->lastName
		));
	}

	/**
	 * @see \Serializable::unserialize()
	 */
	public function unserialize($serialized) {
		list (
				$this->id,
				$email,
				$this->password,
				$this->firstName,
				$this->lastName
		) = unserialize($serialized);
		$this->setEmail($email);
	}
		
	public function __toString() {
		return "$this->lastName, $this->firstName <$this->email>";
	}
	
	public function isEqualTo(UserInterface $m) {
		
		return($m instanceof Member)
			&& (($this->getId() === $m->getId())
			&& ($this->isActive() && $m->isActive()))
			&& ($this->getEmail() === $m->getEmail());
	}
}
