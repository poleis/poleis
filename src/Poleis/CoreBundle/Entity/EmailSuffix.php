<?php
//src/Poleis/CoreBundle/Entity/EmailSuffix

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;

/**
 * @Entity
 * @Table(name="email_suffix")
 *
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 03.16.14
 */

class EmailSuffix extends AuditableEntity {
	private
	/**
	 * @ManyToOne(targetEntity="Poleis\CoreBundle\Entity\School", fetch="EAGER", cascade={"persist"}, inversedBy="emailSuffixes")
	 */
	$school,
	/** @Column(name="email_suffix", type="string") @var string */
	$emailSuffix;
	
	public function __construct() {
		parent::__construct();
	}
	public function setCommunity(School $school) {
		$this->community = $school;
	}
	
	public function getEmailSuffix() {
		return $this->emailSuffix;
	}
	public function setEmailSuffix($emailSuffix) {
		$this->emailSuffix = $emailSuffix;
	}
}
