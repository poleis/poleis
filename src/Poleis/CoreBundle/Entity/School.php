<?php
//src/Poleis/CoreBundle/Entity/School

namespace Poleis\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\ArrayCollection;
use Poleis\CoreBundle\Entity\AbstractEntity\Community;
use Poleis\CoreBundle\Entity\EntityInterface\Permissible;

/**
 * @Entity(repositoryClass="Poleis\CoreBundle\Repository\SchoolRepository")
 * @Table()
 * 
 * @author Timothy Stephens
 * @version 01.05.14
 */

class School extends Community implements Permissible {
	
	private
	/**
     * @OneToMany(targetEntity="Poleis\CoreBundle\Entity\EmailSuffix",
	 * 			  fetch="EAGER", mappedBy="school",
	 * 			  cascade={"all"})
	 * @var ArrayCollection
	 */
	$emailSuffixes;
	public function __construct() {
		parent::__construct();

		$this->emailSuffixes = new ArrayCollection();
	}
	
	public function isSubCommunityOf(Community $community) {
		return false; // false is always a parent community
	}
	public function addMember(Member $member) {
		$this->addMemberHelper($member);
	}
	
	public function getEmailSuffixes() {
		return $this->emailSuffixes;
	}
	public function setEmailSuffixes(ArrayCollection $emailSuffixes) {
		$this->emailSuffixes = $emailSuffixes;
	}
	
	public function addEmailSuffix(EmailSuffix $eS) {
		$eS->setCommunity($this);
		$this->emailSuffixes->add($eS);
	}
	public function removeEmailSuffix(EmailSuffix $eS) {
		$this->emailSuffixes->removeElement($eS);
	}
}