<?php
// src/Poleis/CoreBundle/Form/Type/MemberRegistrationType.php
namespace Poleis\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NicknameType extends AbstractType {
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('nickname', 'text');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Poleis\CoreBundle\Entity\Nickname'
		));
	}

	public function getName()
	{
		return 'Nickname';
	}
}
