<?php
// src/Poleis/CoreBundle/Form/Type/MemberRegistrationType.php
namespace Poleis\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class MemberRegistrationType extends AbstractType {
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('firstName', 'text')
			->add('lastName', 'text')
			->add('email', 'email')
			->add('password', 'repeated', array(
				'first_name'  => 'Password',
				'second_name' => 'Confirm',
				'type'        => 'password'))
			->add('community', 'entity', array(
					'class' => 'PoleisCoreBundle:School',
					'property' => 'name',
					'label' => 'School',
					'empty_value' => 'Choose your school',
					'required' => true,
					'query_builder' => function(EntityRepository $repo) {
			 			return $repo->createQueryBuilder('s')->orderBy('s.name')->where("s.isActive=true");
			})) 
			// should either be set to true if count school > 0 or only render if count school > 0
			// or should be able to input a new school name
			->add('save', 'submit');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Poleis\CoreBundle\Entity\Member'
		));
	}

	public function getName()
	{
		return 'MemberRegistration';
	}
}
