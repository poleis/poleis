<?php
// src/Poleis/CoreBundle/Form/Type/SchoolType.php
namespace Poleis\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SchoolType extends AbstractType {
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text')
			->add('webpage', 'text')
			->add('nicknames', 'collection',
					array('type' => 'Nickname',
						  'allow_add' => true,
						  'allow_delete' => true,
						  'by_reference' => false,
						  'required' => false))
			->add('emailSuffixes', 'collection',
					array('type' => 'EmailSuffix',
						  'allow_add' => true,
						  'allow_delete' => true,
						  'by_reference' => false,
						  'required' => false,
						  'label' => 'Email Suffixes'))
			->add('save', 'submit');
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
				'data_class' => 'Poleis\CoreBundle\Entity\School'
		));
	}

	public function getName()
	{
		return 'School';
	}
}
