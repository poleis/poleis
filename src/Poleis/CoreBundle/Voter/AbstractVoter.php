<?php
// src/Poleis/CoreBundle/Voter/AbstractVoter
namespace Poleis\CoreBundle\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Doctrine\ORM\EntityManager;
use Poleis\CoreBundle\Repository\AbstractRepository;
use Poleis\CoreBundle\Entity\Member;

abstract class AbstractVoter implements VoterInterface {
	private $entityManager, $repository, $supportedClass;
	
	protected function __construct(EntityManager $em, AbstractRepository $repo, $supportedClass = null) {
		$this->entityManager = $em;
		$this->repository = $repo;
		$this->supportedClass = $supportedClass or 'Poleis\CoreBundle\Entity\BasicDBEntity';
	}
	
	/**
	 * Can be overridden if necessary 
	 */
	public function supportsClass($class) {
		return $class === $this->supportedClass || is_subclass_of($class, $this->supportedClass, true);
	}
	/**
	 * Can be overridden if necessary 
	 */
	public function supportsAttribute($attribute) {
		return true;
	}
	
	public function getSupportedClass() {
		return $this->supportedClass;
	}
	
	protected function hasPermission(Member $member, $action, $entity) {
		
	}
}