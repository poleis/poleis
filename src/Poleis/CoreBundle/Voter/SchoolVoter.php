<?php
// src/Poleis/CoreBundle/Voter/SchoolVoter.php
namespace Poleis\CoreBundle\Voter;

use Poleis\CoreBundle\Voter\AbstractVoter;
use Doctrine\ORM\EntityManager;
use Poleis\CoreBundle\Repository\SchoolRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class SchoolVoter extends AbstractVoter {
	
	public function __construct(EntityManager $em, SchoolRepository $repo) {
		parent::__construct ( $em, $repo, 'Poleis\CoreBundle\Entity\School');
	}
	public function vote(TokenInterface $token, $object, array $attributes) {
		if(!$this->supportsClass(get_class($object))) {
			return self::ACCESS_ABSTAIN;
		}
		return self::ACCESS_GRANTED;
	}
}