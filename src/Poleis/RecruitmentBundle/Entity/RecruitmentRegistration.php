<?php
//src/Poleis/RecruitmentBundle/Entity/RecruitmentRegistration.php
namespace Poleis\RecruitmentBundle\Entity;

use Poleis\CoreBundle\Entity\AbstractEntity\AuditableEntity;

use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;

/**
 * @Entity
 * @Table(name="recruitment_registration")
 * @HasLifecycleCallbacks
 * @DoctrineAssert\UniqueEntity(fields="created_by", message="You're already registered!")
 * 
 * @author Timothy Stephens <timothy.f.stephens@gmail.com>
 * @version 03.08.14
 */


class RecruitmentRegistration extends AuditableEntity {
	// creation of a registration is merely a placeholder
	// eg, the created_by member defines both the member
	// and the school at which they are registered
	
	public function __construct() {
		parent::__construct();
	}
}