<?php
//src/Poleis/RecruitmentBundle/Controller.php

namespace Poleis\RecruitmentBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Poleis\CoreBundle\Controller\AbstractController;
use Poleis\CoreBundle\Entity\School;
use Poleis\RecruitmentBundle\Entity\RecruitmentRegistration;

/**
 * @Route("/recruitment")
 * 
 * @author Timothy Stephens <Timothy.F.Stephens@gmail.com>
 * @version 01.08.2014
 */
class RecruitmentController extends AbstractController
{
    /**
     * @Route("/", name="recruitment")
     * @Template("PoleisRecruitmentBundle:Recruitment:index.html.twig")
     */
	public function indexAction() {
		/*
		 * TODO rework all this crap
		 * 
		$comm = $this->getUser()->getCommunities();
		$rval = array("CAN_REGISTER" => 0, "ALREADY_REGISTERED" => 1, "NO_COMMUNITY" => 2, "ALREADY_AFFILIATED" => 3,"status"=>3);
		if ($comm instanceof School) {
			$rval['status'] = ($this->getRepository('Poleis\RecruitmentBundle\Entity\RecruitmentRegistration')->findOneBy(array("createdBy"=>$this->getUser()->getId())))
							? $rval['ALREADY_REGISTERED']
							: $rval['CAN_REGISTER'];
		} else if(empty($comm)) {
			$rval['status'] = $rval['NO_COMMUNITY'];
		} else {
			$rval['status'] = $rval['ALREADY_AFFILIATED'];
		}
		*/
		return array("status" => "Recruitment Controller is currently broken");//$rval;
	}
    
    /**
     * @Route("/register", name="recruitment_register")
     */
    public function registerAction()
    {
    	//TODO should be ajax, return an array of success
    	$this->saveEntity(new RecruitmentRegistration());
    	return $this->redirect($this->generateUrl('recruitment'));
    }

}
