<?php
require_once 'vendor/autoload.php';

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Parser;

function getEntityManager() {
	$yaml = new Parser();
	
	$params = $yaml->parse(file_get_contents("app/config/parameters.yml"))['parameters'];
	
	return EntityManager::create(
			array('driver' => $params['database_driver'],
					'user'	   => $params['database_user'],
					'password' => $params['database_password'],
					'dbname'   => $params['database_name'],
					'port' 	   => $params['database_port'],
					'host'	   => $params['database_host']),
			Setup::createAnnotationMetadataConfiguration(
					array_map( function($path) { return 'src/'.$path; }, $params['entity_paths']),
					$params['dev_mode'])
	);
}
