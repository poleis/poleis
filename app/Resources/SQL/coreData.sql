DROP DATABASE IF EXISTS poleis_acl;
CREATE DATABASE poleis_acl WITH OWNER symfony;

CREATE OR REPLACE FUNCTION buildCoreData() RETURNS void AS $$
DECLARE

default_member       integer := nextval('member_id_seq');
super_admin_member   integer := nextval('member_id_seq');
school_admin_member  integer := nextval('member_id_seq');
org_admin_member     integer := nextval('member_id_seq');
chapter_admin_member integer := nextval('member_id_seq');
regular_member       integer := nextval('member_id_seq');
pnm                  integer := nextval('member_id_seq');

s_default            integer := nextval('community_id_seq');
s_greek_u            integer := nextval('community_id_seq');

o_default            integer := nextval('community_id_seq');
o_gamma_rho_kappa    integer := nextval('community_id_seq');

c_default            integer := nextval('community_id_seq');
c_grk_greek_u_alpha  integer := nextval('community_id_seq');

role_super_admin	 integer := nextval('role_id_seq');

default_password     varchar := '$2y$08$4MzrlfZqHIbDG3siDCDD0OKIbeAqPx.VO4A8uJHNM8yUBZYEa3pPS'; -- 123

BEGIN

-- Members

INSERT INTO member
(id,                   first_name,     last_name,email,                      password,         created_by,     created_date,      updated_by,     updated_date,      active)
VALUES
(default_member,       'Default',      'System', 'default@poleis.com',       default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, false),
(super_admin_member,   'Super',        'Admin',  'super.admin@poleis.com',   default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true),
(school_admin_member,  'School',       'Admin',  'school.admin@poleis.com',  default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true),
(org_admin_member,     'Organization', 'Admin',  'org.admin@poleis.com',     default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true),
(chapter_admin_member, 'Chapter',      'Admin',  'chapter.admin@poleis.com', default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true),
(regular_member,       'Regular',      'Member', 'member@poleis.com',        default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true),
(pnm,                  'Potential',    'Member', 'pnm@poleis.com',           default_password, default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true);

-- Communities

INSERT INTO community
(id,                  name,               email,                  webpage,                 community_type, created_by,           created_date,      updated_by,           updated_date,      active)
VALUES
(s_default,           'Default School',   'default_s@poleis.com', 'www.defaultschool.edu', 'school',       default_member,       CURRENT_TIMESTAMP, default_member,       CURRENT_TIMESTAMP, false),
(s_greek_u,           'Greek University', 'info@greeku.edu',      'www.greeku.edu',        'school',       school_admin_member,  CURRENT_TIMESTAMP, school_admin_member,  CURRENT_TIMESTAMP, true),
(o_default,           'Default Org',      'default_o@poleis.com', 'www.default.org',       'organization', default_member,       CURRENT_TIMESTAMP, default_member,       CURRENT_TIMESTAMP, false),
(o_gamma_rho_kappa,   'Gamma Rho Kappa',  'info@grk.org',         'www.gammarho.org',      'organization', org_admin_member,     CURRENT_TIMESTAMP, org_admin_member,     CURRENT_TIMESTAMP, true),
(c_default,           'Default Chapter',  'default_c@poleis.com', 'www.defaultchapter.org','chapter',      default_member,       CURRENT_TIMESTAMP, default_member,       CURRENT_TIMESTAMP, false),
(c_grk_greek_u_alpha, 'GRK Alpha',        'alpha@grk.org',        'www.gammarho.org/alpa', 'chapter',      chapter_admin_member, CURRENT_TIMESTAMP, chapter_admin_member, CURRENT_TIMESTAMP, true);

INSERT INTO school (id) VALUES (s_default), (s_greek_u);
INSERT INTO organization (id) VALUES (o_default), (o_gamma_rho_kappa);
INSERT INTO chapter (id, org_id, school_id) VALUES (c_default, o_default, s_default), (c_grk_greek_u_alpha, o_gamma_rho_kappa, s_greek_u);

-- Membership

INSERT INTO community_membership
(community_id, member_id)
VALUES
(s_default, default_member),
(o_default, default_member),
(c_default, default_member),
(s_greek_u, super_admin_member),
(s_greek_u, school_admin_member),
(s_greek_u, org_admin_member),
(s_greek_u, chapter_admin_member),
(s_greek_u, regular_member),
(s_greek_u, pnm),
(o_gamma_rho_kappa, super_admin_member),
(o_gamma_rho_kappa, org_admin_member),
(o_gamma_rho_kappa, school_admin_member),
(o_gamma_rho_kappa, chapter_admin_member),
(o_gamma_rho_kappa, regular_member),
(o_gamma_rho_kappa, pnm),
(c_grk_greek_u_alpha, super_admin_member),
(c_grk_greek_u_alpha, school_admin_member),
(c_grk_greek_u_alpha, org_admin_member),
(c_grk_greek_u_alpha, chapter_admin_member),
(c_grk_greek_u_alpha, regular_member),
(c_grk_greek_u_alpha, pnm);

-- Roles
INSERT INTO role
(id,			   role,			   name,		  parent_id, 		root,			  level, left_pos, right_pos, created_by, 	  created_date, 	 updated_by, 	 updated_date, 		active)
VALUES
(role_super_admin, 'ROLE_SUPER_ADMIN', 'Super Admin', role_super_admin, role_super_admin, 0,	 0,		   0,		  default_member, CURRENT_TIMESTAMP, default_member, CURRENT_TIMESTAMP, true);

END;
$$ LANGUAGE plpgsql;

SELECT buildCoreData();

