echo 'Refreshing Database...';
php bin/doctrine orm:schema:drop --force && php bin/doctrine orm:schema:create && echo 'Inserting Default Data...' && psql -d poleis_dev -U symfony -f app/Resources/SQL/coreData.sql;
echo 'Creating ACL tables...'
php app/console init:acl;
